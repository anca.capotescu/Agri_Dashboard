# Agri_Dashboard
## Codul sursă se poate găsi la adresa https://gitlab.upt.ro/anca.capotescu/Agri_Dashboard/-/tree/master

## Descărcarea proiectului
$ git clone https://gitlab.upt.ro/anca.capotescu/Agri_Dashboard.git

## Instalarea pachetelor necesare
$ npm install # sau npm i 

## Lansarea aplicației
$ npm run dev
